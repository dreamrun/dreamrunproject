<!DOCTYPE html>
<html>
  <head>
  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Online Book store</title>
    
    <!-- Bootstrap -->
   
  <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/galleriffic-2.css" type="text/css" />

<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.min.css" />





    <style type="text/css">
	  .bod
	  {
		  background-color:#fff;
	  }
	   .cont
	   {
		   background-color:#fff;
		   border:0px solid;
		   width: 967px;
	   }
	   .aprofile
	   {
		   border:1px solid; 
		   margin-left:30px; 
		   background-color:#fff; 
		   box-shadow: 4px 2px 5px;
		   height: 115px;
		   border-radius:5px;"
	   }
	   .mybooks
	   {
		   border:1px solid;
		   margin-left:24px;
		   background-color:#fff; 
		   box-shadow: 4px 2px 5px;
		   height: 115px;
		   border-radius:5px;"
	   }
	   .addbook
	   {
		   border:1px solid;
		   background-color:#fff; 
		   box-shadow: 4px 2px 5px;
		   height: 115px;
		   border-radius:5px;"  
	   }
	   .dashboard
	   {
		   margin-top:20px;
	   }
	   .prof
	   {
		    background: #fff;
			width: 76px;
			height: 76px;
			float: left;
			margin: 19px 0px 0px 10px;
			border: 1px solid;
			border-radius: 6px;
	   }
	   .profdetil
	   {
		    float: left;
			margin-left: 30px;
			margin-top: 2px;
			font-family: vijaya;
			font-size: 28px;
			font-weight: bold;
	   }
	   
	   
	   
	   .books
	   {
		    background: #fff;
			width: 76px;
			height: 76px;
			float: left;
			margin: 19px 0px 0px 10px;
			border: 1px solid;
			border-radius: 6px;
	   }
	   .bookdetil
	   {
		    float: left;
			margin-left: 30px;
			margin-top: 2px;
			font-family: vijaya;
			font-size: 28px;
			font-weight: bold;
	   }
	   
	   .abooks
	   {
		    background: #fff;
			width: 76px;
			height: 76px;
			float: left;
			margin: 19px 0px 0px 10px;
			border: 1px solid;
			border-radius: 6px;
	   }
	   .abookdetil
	   {
		    float: left;
			margin-left: 30px;
			margin-top: 2px;
			font-family: vijaya;
			font-size: 28px;
			font-weight: bold;
	   }
	   
	  .error_script{
		   
		 color:#F00;
		    
		} 
	</style>
    
    
 
 <!--   <script type="text/javascript" src="js/jquery-1.1.3.1.min.js"></script>-->
 <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
<script>  
  function display_modal()
{
	$('#fblink').modal('show');
}
</script>

<!-------------- this is for showing buttons ------------------------>
<script type="text/javascript">
function showhide()
{
	$("#author_profile").show();
	$("#mybooks").hide()
	
}
function showhideoff()
{
	$("#mybooks").show();
	$("#author_profile").hide()
	
}

</script>
<!---------------------------- end of it------------------------->


<!----------------- form validation -------------------------> 
 
 <script type="text/javascript">
 	
 
      function validate_form()
	  {
		var booktitle=$("#booktitle").val(); 
		var authorname=$("#authorname").val();
		var publisher=$("#publisher").val();
		var bookdescription=$("#bookdescription").val();
		var keywords=$("#keywords").val();
		var checkbox=$('#ebook').val();
		var checkbox1=$('#paperbag').val();
		var checkbox2=$('#hardcore').val();
		var genre=$('#genre').val();
		
		$('#booktitle_error').empty();
		$('#authorname_error').empty();
		
		$('#publisher_error').empty();
		$('#bookdescription_error').empty();
		
		$('#keywords_error').empty();
		$('#checkbox_error').empty();
		$('#genre_error').empty();
		
		if(booktitle=='')
		{
			$("#booktitle_error").html('This field is required!!');
			return false;
		}
		if(authorname=='')
		{
			$("#authorname_error").html('Author Name is required');
			return false;
		}
		if(booktitle!='' && booktitle.length<3)
		{
			$("#booktitle_error").html(' Book Title should have 3 characters');
			return false;
		}
		if(authorname!=='' && authorname.length<3)
		{
			$("#authorname_error").html('authorname should have 3 characters');
			return false;
		}
		
		if(publisher=='')
		{
			$("#publisher_error").html('Publisher field is required');
			return false;
		}
		if(genre=='')
		{
			$("#genre_error").html('This field is required');
			return false;
		}
		if(bookdescription=='')
		{
			$("#bookdescription_error").html('Book Description field is required');
			return false;
		}
		
		if(keywords=='')
		{
			$("#keywords_error").html('keyword field is required');
			return false;
		}
		if(keywords!='' && keywords.length<10)
		{
			$("#keywords_error").html('Minimun 3 characters');
			return false;
		}
		
		if(!$('#checkbox').prop('checked') ||!$('#checkbox1').prop('checked')||!$('#checkbox2').prop('checked'))
		{
			//alert($('#checkbox').prop('checked'));
			$('#checkbox_error').html('select the checkbox');
			return false;
			}
				
	   }
  
  
 
 </script>
   
  <!---- end of it ------------------------>  
    



  
  </head>
  <body>
    
        <div class="container-fluid bod" >
        
           <div class="container cont">
               <div class="row">
                  <h1 style="font-family:vijaya; color:#000; text-align:center;font-size:30px;">Book Enthusiast Dashboard</h1>
                  
               </div>
               
               
               <div class="row dashboard">
               
                 <div class="span4 aprofile" style="width:295px;height:100px;">
                 
                     <div class="author" style="width:97px;height:113px;border:0px solid;float:left;">
                     <div class="prof">
                          <img src="images/profile.png" width="80" height="80" id="profi">
                          
                     </div>
                     </div>
                     
                     <div class="detil" style="float:left; width:198px;height:100px;">
                     
                     <a href="javascript:;" onClick="showhide()"><span class="profdetil">Author Profile</span>
                     <p style="float:left;padding: 0px 0px 0px 26px;">Edit and View Profile </p>
                     </a>
                     
                     </div>
                     
                 
                 </div>
                 
                 
                 <!-----------=============================End of the profile=======================------------------>
                 
                 
                  <div class="span4 mybooks" style="width:300px;height:100px;">
                  
                  <div class="book" style="width:97px;height:113px;border:0px solid;float:left;">
                  
                     <div class="books">
                          <a href="javascript:;" onClick="showhideoff()"><img src="images/books_icon_1.png" width="80" height="80" id="book"></a>
                     </div>
                     </div>
                     
                     <div class="book1" style="float:left;width:198px;height:100px;">
                     
                     <a href="javascript:;" onClick="showhideoff()"><span class="bookdetil">My Books</span></a>
                  
                     </div>
                 </div>
                 
                 
                 
                 <!-----------=============================End of the Mybooks=======================------------------>
                 
                 
                  <div class="span4 addbook" style="width:310px;height:100px;">
                  
                  <div class="adbook" style="width:97px;height:113px;border:0px solid;float:left;">
                  
                  <div class="abooks">
                          <a href="#"><img src="images/books_blue_add.png" width="80" height="80" id="a_book"></a>
                     </div>
                     
                     </div>
                     
                     
                     <div class="addetil" style="float:left;width:198px;height:100px;">
                         <a href="#"><span class="abookdetil">Add A Book</span></a>
                     </div>
                 </div>
                 
                   <!-----------=============================End of the Addbook=======================------------------>
           
               </div>
               <!--------------------End of the Dashboard----------> 
               
               <!---- this is for profile of author --->
               
                <div class="container cont" style="border:1px solid; margin-top:20px; border-radius:5px;">
          
             
               <div class="row" id="author_profile" style="margin-top:30px;">
               				
             
                       <?php /*?><?php
					      
						  if($msg=='error')
						  {
							  echo '<p style="color:#F00;">This User Already Exist in Database!!!</p>';
						   }
					   
					   
					   ?><?php */?>
                    <h3 class="text-center" style="color:green; font-family:vijaya;"> Author Profile</h3>
                    
                    <div class="span9 offset1" style="margin-top:10px;">
                    <form class="form-horizontal" action="#" method="post">
                    
                   
                            <div class="control-group"><!---------=====Selection type ========------------------>
                            
                                <label class="control-label" for="name">I'm a/an</label>
                                
                                    <div class="controls">
                                    
                                        <select name="user_type" id="user_type" >
                                        
                                            <option selected="selected" value="">Please make a selection</option>
                                            
                                            <option value="enthusiast">Book Enthusiast-Reader</option>
                                            
                                            <option value="author">Author</option>
                                            
                                        </select>
                                      
                                   
                                    </div>
                            
                            </div>
                            
                            
                            <div class="control-group"><!---------=====First Name ========------------------>
                            
                               <label class="control-label" for="first_name" >FirstName :</label>
                               
                               	<span style="color:#F00; float:left; padding-top:10px; padding-left:3px;">*</span>
                                
                                    <div class="controls">
                                    
                                    	<input type="text" name="firstname" id="first_name" /> 
                                        
                                       
                                    
                                    </div>
                            
                            </div>
                            
                            <div class="control-group"><!---------=====LastName ========------------------>
                            
                                <label class="control-label" for="lastname">LastName :</label>
                                
                                		<span style="color:#F00; float:left; padding-top:10px; padding-left:3px;">*</span>
                                
                                    <div class="controls">
                                    
                                  	  	<input type="text" name="lastname" id="last_name"/>
                                        
                                      
                                    
                                    </div>
                            
                            </div>
                            
                            <div class="control-group"><!---------=====Email Addresss ========------------------>
                            
                                <label class="control-label" for="email-id">Email Address :</label>
                                
                                		<span style="color:#F00; float:left; padding-top:10px; padding-left:3px;">*</span>
                                
                                    <div class="controls">
                                    
                                    	<input type="text" name="email_address" id="email_id" />
                                        
                                      
                                    </div>
                                
                            </div>
                            
                            <div class="control-group"><!---------=====Confirm Email Addresss========------------------>
                            
                                <label class="control-label" for="confirm-email">Confirm Email Address :</label>
                                
                                		<span style="color:#F00; float:left; padding-top:10px; padding-left:3px;">*</span>
                                
                                    <div class="controls">
                                    
                                    	<input type="text" name="confirm-email-id" id="confirm_mailid" />
                                        
                                       
                                    
                                    </div>
                                
                            </div>
                            
                            
                            <div class="control-group"><!---------=====Password ========------------------>
                            
                                <label class="control-label" for="password">Password :</label>
                                
                                		<span style="color:#F00; float:left; padding-top:10px; padding-left:3px;">*</span>
                                
                                    <div class="controls">
                                    
                                    	<input type="password" name="password" id="_password"/>
                                     
                                    
                                    </div>
                            
                            </div>
                            
                            
                            <div class="control-group"><!---------=====Confirm Password ========------------------>
                            
                                <label class="control-label" for="confirm-password">Confirm Password :</label>
                                
                                		<span style="color:#F00; float:left; padding-top:10px; padding-left:3px;">*</span>
                                
                                    <div class="controls">
                                    
                                    	<input type="password" name="confirm-password" id="confirm_password"/>
                                        
                                         
                                    </div>
                                    
                            </div>
                            
                            <br/>
                            
                            <div class="control-group"><!---------=====How did you hear ========------------------>
                                
                                <label class="control-label" for="resource from ?">How did you hear about us? :</label>
                                   
                                    <div class="controls">
                                    
                                    	<input type="text" name="how_did_here" id="site_source_type"/>
                
                                    </div>
                                
                            </div>
                    
                      </form>
                      </div>
                      
                      
                      
                      
                     </div>
                       
                    </div>
      
               
               
               </div>
               
               </div>
               <!---- end of the profile of author -------------->
               
               
               
               <!---=========================- this is for my books button ================================================------>
        <div id="mybooks" style="display:none;">       
               
         <div class="row" style="margin-top:55px;">
         
         <div class="span8 bookshows offset2" style="background-image:url(images/list%20of%20books.png);width:829px;height:129px; border:1px solid #EEE;">
              
              
              <div class="img1" style="margin-left:10px;">
              
                 <img src="PNGIMAGES/2933-large.png" width="70" height="112" style="float:left;margin:9px 0px 0px 0px;">
              </div>
              
              <span style="margin-left:33px;line-height:35px;font-family:vijaya;font-size:21px;font-weight:bold;">Painted Faces</span>
              
              
              <!-----------=================End of the book=================-------------->
              
              <div class="btns" style="float:right;width:155px; height:129px; border:0px solid;">
              
               <a href="javascript:;" onclick="display_modal()"><div class="btn1" style="background-color:#c4c4c4;width:146px; height:46px; border-radius:5px; margin-left:5px; margin-top:9px;">
                          
                   <img src="images/1377951437_system-software-update.png" height="45" width="45" style="float:left;">  
         <span class="up" style="color:#000;font-family:vijaya;font-size:20px;margin-left:11px;font-weight:bold;line-height:40px;">Update</span>     
              </div></a>
              
              
              
              <a href="book_details.php"><div class="btn2" style="background-color:#c4c4c4;width:146px; height:46px; border-radius:5px; margin-left:5px; margin-top:18px;">
                          
                   <img src="images/preview.png" height="45" width="45" style="float:left;">  
      <span class="up1" style="color:#000;font-family:vijaya;font-size: 20px;margin-left: 11px;font-weight: bold;line-height: 40px;">Preview</span>     
              </div></a>
              
              
              </div>
              <!---==========================End of the btnspart=====================---------->
                
                        
          </div>
            <!----------------------End of the row-------------------------->
            
          </div>
          <!---========================End of the bookshows==================------------->
          
          
          
          
            <div class="row" style="margin-top:55px;" >
         
         <div class="span8 bookshows offset2" style="background-image:url(images/list%20of%20books.png);width:829px;height:129px; border:1px solid #EEE;">
              
              
              <div class="img1" style="margin-left:10px;">
              
                 <img src="PNGIMAGES/2933-large.png" width="70" height="112" style="float:left;margin:9px 0px 0px 0px;">
              </div>
              
              <span style="margin-left:33px;line-height:35px;font-family:vijaya;font-size:21px;font-weight:bold;">Painted Faces</span>
              
              
              <!-----------=================End of the book=================-------------->
              
              <div class="btns" style="float:right;width:155px; height:129px; border:0px solid;">
              
               <a href="javascript:;" onclick="display_modal()"><div class="btn1" style="background-color:#c4c4c4;width:146px; height:46px; border-radius:5px; margin-left:5px; margin-top:9px;">
                          
                   <img src="images/1377951437_system-software-update.png" height="45" width="45" style="float:left;">  
    <span class="up" style="color:#000;font-family:vijaya;font-size: 20px;margin-left: 11px;font-weight: bold;line-height: 40px;">Update</span>     
              </div></a>
              
              
              
             <a href="book_details.php"> <div class="btn2" style="background-color:#c4c4c4;width:146px; height:46px; border-radius:5px; margin-left:5px; margin-top:18px;">
                          
                   <img src="images/preview.png" height="45" width="45" style="float:left;">  
                   <span class="up1" style="color:#000;font-family:vijaya;font-size: 20px;margin-left: 11px;font-weight: bold;line-height: 40px;">Preview</span>     
              </div></a>
              
              
              </div>
              <!---==========================End of the btnspart=====================---------->
                
                        
          </div>
            <!----------------------End of the row-------------------------->
            
          </div>
          <!---========================End of the bookshows==================------------->
                     
      </div>  
      <!-------------------------------- end of my booooks button ------------------------------------------>  
        
        
        
        
        
        
        </div><!----End of the Cotainer--------->
        
        </div>
      <!----------------End of the body-fluid----------------->
        
     <!--modal for enlarge--->

   <div class="modal hide fade" id="fblink"  aria-labelledby="modalLabel" aria-hidden="true" style="width:700px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Book details</h4>
        </div>
        
        <div class="modal-body">
        	<form id="socaillinks" method="post" action="" onSubmit="return validate_form();" class="form-horizontal" enctype="multipart/form-data">
            		
           				<div class="control-group">
                 	<label >Book Title: <span style="color:#F00;">*</span></label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input id="booktitle" type="text" name="booktitle" />
                    <span id="booktitle_error" class="error_script"></span>
                    </div>
                    </div>
                    <div class="control-group">
                 	<label >Author Name: <span style="color:#F00;">*</span></label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input id="authorname" type="text" name="authorname"  />
                     <span id="authorname_error" class="error_script"></span>
                    </div>
                    </div>
                    
                   
                   <div class="control-group">
                 	<label >13-Digit ISBN</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input id="13ISBN" type="number" name="13ISBN" validate="required" maxlength=13 />
                    
                    </div>
                   </div>
                   <div class="control-group">
                 	<label >10-Digit ISBN</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input id="10ISBN" type="number" name="10ISBN" validate="required" maxlength=10 />
                    
                    </div>
                   </div>
                   <div class="control-group">
                 	<label >Amazon ASIN</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input id="amazon" type="text" name="amazon" validate="required"  />
                    
                    </div>
                   </div>
                   
                   <div class="control-group">
                 	<label >Publisher<span style="color:#F00;">*</span></label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input id="publisher" type="text" name="publisher" />
                     <span id="publisher_error" class="error_script"></span>
                    
                    </div>
                   </div>
            
            <div class="control-group">
                 	<label >Publisher Website Address:</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input id="publisherwebsite" type="text" name="publisherwebsite" validate="required" />
                    
                    </div>
                   </div>
                   
                   <div class="control-group">
                 	<label >Genre(Primary):<span style="color:#F00;">*</span></label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                   <select  id="genre" validate="required" name="genre">
                       <option value="" validate="required">genre select</option>
                       <option value="comedy"validate="required">comedy</option>
                       <option value="fiction" validate="required">fiction</option>
                   </select>
                     <span id="genre_error" class="error_script"></span>
                    
                    </div>
                   </div>
                   
                   <div class="control-group">
                 	<label >Genre(2nd-Optional):</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                   <select  id="genre2" validate="required" name="genre2">
                       <option value="" validate="required">genre select</option>
                       <option value="comedy"validate="required">comedy</option>
                       <option value="fiction" validate="required">fiction</option>
                   </select>
                    
                    </div>
                   </div>
                   
                   <div class="control-group">
                 	<label >Genre(3rd-Optional):</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                   <select  id="genre3" validate="required" name="genre3">
                       <option value="" validate="required">genre select</option>
                       <option value="comedy"validate="required">comedy</option>
                       <option value="fiction" validate="required">fiction</option>
                   </select>
                    
                    </div>
                   </div>
                   
                   <div class="control-group">
                 	<label >Book Description:<span style="color:#F00;">*</span></label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                   <textarea id="bookdescription" name="bookdescription" maxlength="1500" placeholder="Allowed 1500 characters" style="resize:none" ></textarea>
                    <span id="bookdescription_error" class="error_script"></span>
                    
                    </div>
                   </div>
                   
                   <div class="control-group">
                 	<label style="margin-top: 20px;" >Add keywords About Book(Min 3):<span style="color:#F00;margin-top:20px;">*</span></label>
                   	<div class="controls" style="margin-top:-20px; margin-left:220px;">
                   <textarea id="keywords" name="keywords" placeholder="seperated keywords with comma" style="resize:none"></textarea>
                     <span id="keywords_error" class="error_script"></span>
                    </div>
                   </div>
                   
                   <div class="control-group">
                 	<label style="margin-top:10px;" >Format (select all that apply):<span style="color:#F00;margin-top: 20px;">*</span></label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                    <input type="checkbox" name="format" id="ebook" style="margin-left:5px;"><span style="padding-left:5px;">Ebook</span>
                    <input type="checkbox" name="format" id="paperbag" style="margin-left:5px;"><span style="padding-left:5px;">Paperbag</span>
                    <input type="checkbox" name="format" id="hardcore" value="hardcore" style="margin-left:5px;"><span style="padding-left:5px;">Hardcover</span>
                     <span id="checkbox_error" class="error_script"></span>
                    </div>
                   </div>
            		
                    <div class="control-group">
                 	<label >Authors Bio:</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                   <textarea id="authorbio" name="authorbio" style="resize:none"></textarea>
                    
                    </div>
                   </div>
                   
                    <div class="control-group">
                 	<label  style="margin-top:10px;">Upload Author's Image</label>
                   	<div class="controls" style="margin-top:-20px;margin-left:220px;">
                  <input id="image" type="file" name="image" validate="required" style="width:197px;" />
                    
                    </div>
                   </div>
          
            <input type="submit" class="btn btn-info" style="margin-left:400px;" value="ADD DETAILS" />
            </form>
             
        
       
      </div><!-- /.modal-body -->
      </div><!-- modal content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<!-- end of it -->
    
    
    
  
  </body>
</html>