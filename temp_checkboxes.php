<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Online Book store</title>
    
    <!-- Bootstrap -->
   
  <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/galleriffic-2.css" type="text/css" />

<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.min.css" />

<script src="js/jquery-1.10.2.js" type="text/javascript"></script>


<script>
	
	function check_all_checkbox()
	{
	
		if($('#checkbox_id').prop('checked')){
				$('.value_basic').each(function()
				{
			
					$(this).prop('checked','checked');
				
				});
		}
		
	}
	
	function uncheck_last()
	{
		var arr=[];
		       $('.value_basic').each(function()
				{
					
					if($(this).prop('checked'))
					{
						arr.push($(this).val());
					}
					
				});
				//alert(arr.length);
				if(arr.length==10)
				{
						$('#checkbox_id').prop('checked','checked');
				}
				else
				{
					
							$('#checkbox_id').prop('checked', false);
				}
		
	}
	
</script>
<style>
		.book_heading
		{
			margin-top:30px;
			margin-left:90px;
			font-size:18px;
			font-family:"Arial Black", Gadget, sans-serif;
		}
		
		
		.chk_option_styles
		{
			margin-top:10px;
			margin-left:90px;
			font-size:18px;
			font-family:Arial, Helvetica, sans-serif;
		}
		.sub_book_heading
		{
			font-size:12px;
			margin-left:90px;
			font-family:Arial, Helvetica, sans-serif;
		}
	     .upload_button
		 {
			 margin-top:40px;
			 margin-left:400px;
		 }
			
			
</style>

</head>
<body>

   <div class="tab-content">
                    
                            <div class="form_class">
                            
                                
                                <form class="form-horizontal" action="add_book_step1.php" method="post">
                                
                                 <div class="free_book">
                                    
                                    <div class="book_heading"> <!---=====-FREE Book Services---=======--->
                                    
                                        <span>Author Hype FREE Book Promotion Services</span>
                                    
                                    </div>
                                    
                                
                                    <div class="chk_option_styles">
                                      
                                            <div class="controls">
                                            
                                                <label class="checkbox" >
                                                
                                             <input type="checkbox" value="yes"  class="value_basic" checked="checked" onclick="uncheck_last();" name="free_book" id="free_book"/>Add your Book details to Author Hype for FREE!  We will setup a Book Promotion Page on our website that receives thousands of visitors every month looking for the best new author's !
                                                
                                                </label>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                 </div>
                                    
                                    
                                 <div class="value_book">
                                    
                                    <div class="book_heading"><!---=====-VALUE Book Services---=======--->
                                    
                                        <span>Author Hype Value Book Promotion Services ($5 for each service)</span>
                                    
                                    </div>
                                    
                                    <div class="sub_book_heading">
                                    
                                    	<span>Question about the Value Promotion Services?  Check out our FAQ page for more information</span>
                                    
                                    </div>
                                    
                                    
                                    
                                    <div class="chk_option_styles">
                                    
                                         <div class="control-group">
                                    
                                            <div class="controls">
                                            
                                                <label class="checkbox">
                                                
                                    <input type="checkbox" value="yes" class="value_basic" onclick="uncheck_last();"  name="v_author_website_fb_tw" id="v_author_website_fb_tw" />Add a link to the Authors Website, Facebook page, and Twitter Page
                                                </label>
                                              
                                            </div>
                                    
                                   		 </div>
                                    
                                         <div class="control-group">
                                         
                                             <div class="controls">
                                            
                                                <label class="checkbox">
                                                
                                                 <input type="checkbox" value="yes" class="value_basic" onclick="uncheck_last();"  name="v_quarterly_ah_awards" id="v_quarterly_ah_awards"/>Enter your book into the Quarterly Author Hype Awards
                                                </label>
                                             
                                            </div>
                                        
                                        </div>
                                   
                                     <div class="control-group">
                                     
                                         <div class="controls">
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox" value="yes" class="value_basic" onclick="uncheck_last();"  name="v_google_book_preview" id="v_google_book_preview"/ >Add a Google Books Preview to your Author Hype Book Promotion Page!  Please note your book must be listed on Google Books and have an ISBN # - Please <a href="www.google.com">click here </a>for more information
                                                
                                                </label>
                                           
                                            </div>
                                           
                                        </div>
                                        
                                         <div class="control-group">
                                         
                                            <div class="controls">
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox" class="value_basic" value="yes" onclick="uncheck_last();"  name="v_purchase_amazon" id="v_purchase_amazon"/>Add your book to over 10 unique websites that will drive readers to purchase your book on Amazon.com
                                                
                                                </label>
                                               
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="control-group">
                                         
                                            <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox" class="value_basic" value="yes" onclick="uncheck_last();"  name="v_unique_review" id="v_unique_review"/>We will write a unique review of your book on Author Hype, Amazon.com, & Good Reads (max length - paragraph)
                                                
                                                
                                                </label>
                                               
                                            </div>
                                        
                                        </div>
                                        
                                         <div class="control-group">
                                         
                                             <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  class="value_basic" value="yes" onclick="uncheck_last();" name="v_bloggers_reviewers" id="v_bloggers_reviewers"/>Send your book to 20 independent bloggers/reviewers that potentially will write a review on Amazon.com, Good Reads, their personal blog, and others!
                                                </label>
                                             
                                             </div>
                                               
                                        </div>
                                        
                                         <div class="control-group">
                                         
                                            <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  class="value_basic" value="yes" onclick="uncheck_last();" name="v_prmotional_tweet" id="v_prmotional_tweet"/>Send out one promotional tweet to over 50,000 followers  
                                                
                                                </label>
                                               
                                            </div>
                                        
                                        </div>
                                        
                                         <div class="control-group">
                                         
                                            <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  class="value_basic" value="yes" onclick="uncheck_last();" name="v_email_blast" id="v_email_blast"/>Included in our email blast to our opt-in Book Enthusiast List (Text link) 
                                                
                                                </label>
                                               
                                            </div>
                                        
                                        </div>
                                        
                                         <div class="control-group">
                                         
                                             <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  class="value_basic" value="yes" onclick="uncheck_last();" name="v_frontpage_week" id="v_frontpage_week"/>Featured on front page of Author Hype for One Week! (Book Cover and Title with link to Book Promotional Page - lower 1/2 of page)
                                                
                                                </label>
                                             
                                              </div>
                                            
                                        </div>
                                        
                                   		 <div class="control-group">
                                         
                                             <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  onclick="check_all_checkbox();" id="checkbox_id" name="services"/>Select all services above and pay only $40 - A $5 savings!
                                                </label>
                                               
                                            </div>
                                        
                                        </div>
                                        
                             </div>
                                        
                                        
                                 <div class="preminum_book">
                                        
                                     <div class="book_heading"><!---=====-PREMIUM Book Services---=======--->
                                
                                    		<span>Author Hype Book Premium Promotion Services </span>
                                
                                	</div>
                                    
                                     <div class="sub_book_heading">
                                    
                                    	<span>Question about the Premium Promotion Services?  Check out our FAQ page for more information</span>
                                    
                                    </div>
                                    
                                    <div class="chk_option_styles">
                                    
                                     <div class="control-group">
                                     
                                     	<div class="controls" style="height:55px;">
                                                
                                                   <label class="checkbox"> <input type="checkbox"  class="premium_basic" name="p_static_ad_banner" id="p_static_ad_banner" value="yes"/>$849.99 - Static Ad Banner 440 PX (W) x 125 PX (H) on a daily book trade newsletter.  This is email newsletter is dedicated to helping people in the book industry make decisions about buying, selling and lending books.  The list consists of 30,000 book trade readers mainly consisting of booksellers, librarians and publishing professionals. Of the booksellers, many are independents, but the buyers at B&N and Amazon read the email newsletter daily as well.  Also included in this package is the layout and design of the static ad.  </label>
                                       
                                     </div>
                                     
                                     </div>
                                     
                                        <div class="control-group">
                                           
                                            <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  class="premium_basic" name="p_own_author_website" id="p_own_author_website" value="yes"/>$299.99 - Design, develop, build, and host (1 year) your very own custom 5 page Author Website.  As the author you are the brand and your books are the products.  It is critical to have a professional website to promote your brand!
                                                
                                                </label>
                                               
                                            </div>
                                          
                                          </div>
                                          
                                           <div class="control-group">
                                   
                                           <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  class="premium_basic" name="p_weekly_newsletter" id="p_weekly_newsletter" value="yes"/>$199.99 - Featured your book in our weekly newsletter that goes out to our Book Enthusiast looking for new up and coming Author's!  The $149.99 price includes one placement per quarter for a period of 1 year.
                                                </label>
                                               
                                            </div>
                                      
                                        </div>
                                        
                                 <div class="control-group">
                                 
                           
                                   <div class="controls">
                                    
                                    
                                        <label class="checkbox">
                                        
                                            <input type="checkbox"  class="premium_basic" name="p_author_pressrelease" id="p_author_pressrelease " value="yes"/>$179.99 - Author Press Release Package - Author Hype will write and release your press release.  Don't pay as much as $450 for this service elesewhere! 
                                        
                                        </label>
                                       
                                    </div>
                              
                                </div>
                                        
                                     <div class="control-group">
                                     
                                          <div class="controls">
                                            
                                            
                                                <label class="checkbox">
                                                
                                                    <input type="checkbox"  class="premium_basic" name="p_social_mdeia" id="p_social_mdeia" value="yes"/>$99.99 - Social Media campaign blitz! 
                                                
                                                </label>
                                               
                                            </div>
                                            
                                    </div>
                                   
                                  <div class="control-group">
                                  
                                     <div class="controls">
                                        
                                        
                                            <label class="checkbox">
                                            
                                                <input type="checkbox"  class="premium_basic" name="p_front_page_60days" id="p_front_page_60days" value="yes"/>$49.99 - Feature Your Book on the Author Hype Front Page - Cover Image & Title - 60 days - Top 1/4 of paqge that is seen by 1,000's of visitors daily!  
                                            
                                            </label>
                                           
                                        </div>
                                        
                                    </div>
                                    
                                      <div class="control-group">
                                  
                                             <div class="controls">
                                                
                                                
                                                    <label >
                                                    
                                                        Coming Soon - Feature your book on an Author Hype Radio Commercial  
                                                    
                                                    </label>
                                                   
                                                </div>
                                        
                                    	</div> 
                                        
                                        
                                            
                                                                    
                                    </div>
                                    
                                    <div class="upload_button">
                                            
                                            <div class="controls"><!---------=====Purchase & uplaod ========------------------>
                                
                                                <input class="btn" type="submit"  value="Purchase & Upload Book"/>
                                
                                             </div>
                                             
                                 		</div>
                                            
                                    
                                    
                                    </div>
                            
                            </div>
                                </form>
                                
                            </div>
                    
                    </div>



</body>
</html>