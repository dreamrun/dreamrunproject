

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head><title>
	Published.com - A Free Book Directory. Discover The Best Books Today.
</title>
<meta name="keywords" content="book directory, best books, free book directory, the best books"><meta name="description" content="Published.com is a free book directory dedicated to book promotion of the best books. Helping authors and publishers get their books discovered.">
    
    <!--<script src="/js/jquery-1.3.2.min.js"></script>-->
    <script src="temp/jquery.min.js"></script> 
    <script src="temp/jquery.tools.min.js"></script>    

    <!-- Page Styling -->
    <link href="temp/public.css" rel="stylesheet" type="text/css" /><link href="temp/homepage.css" rel="stylesheet" type="text/css" /><link href="temp/scroll.css" rel="stylesheet" type="text/css" /></head>
<body>
    <form name="form1" method="post" action="default.aspx" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'topgnav1_search_submit')" id="form1">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJNDU1NDUzNjkzZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAQUWdG9wZ25hdjEkc2VhcmNoX3N1Ym1pdCAvLFZFH6TZj0Crlz2igPTYs64E" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=xsJMpp7kNlfvAJh-YtJkfq4fceyGXz4C_X1L-QkjzudBr6RT1BNLlfMeta1IpgBTz6Yw-p1fjRSsBn3SpPTM_7YXiKM1&amp;t=634604208479085897" type="text/javascript"></script>

<div>

	<input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="b8KjQzh6yDNO-uJ00aC9WK3DPfZR4AVR8Goh0_ki-VB-1T-VOqW_VHZmQLwtYxKg8_qGsCD9k55BkQxtaCA8iFp-wmU1" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWAgKOoOygDgK9s8qRBizlKVOTPS5m1+k76YKo+hI321Jb" />
</div>
            
    <div id="main-bg">
        <div id="m_container">               
            <div style="position: relative; width: 100%;height: 588px; border: solid 0px #ff0d0d;">
            
                <div style="position:absolute;top:122px;width:1006px;height:500px; background-image: url('/images/bk2.jpg');">
                </div>
                
                <!-- Top content area **************************************************************************** -->                
                <div id="content1">                
                    <span class=homeheader>Discover The Best Books Right Now!</span>
                    <p class="med_black">Since 1997, the Published.com Book Directory has been helping book lovers discover the best books.  We've also made book promotion free and easy for all authors and publishers, big and small.  Now, we're kicking book promotion up at least three notches.</p>                    
                    <a href="/how-published-works.aspx" ><img src="/images/books-found-free.png" border=0 /></a><br />
                    <a href="/published-search.aspx"><img src="/images/search-books.png" border=0 /></a>
                </div>                
            </div>
            <!-- Middle Buttons **************************************************************************** -->
            <div id="middle-buttons">
                <table width="100%" border=0 cellpadding=0 cellspacing=0>
                <tr>
                    <td><a href="/about.aspx"><img src="/images/published-about.png" border=0 /></a></td>
                    <td><a href="/how-published-works.aspx"><img src="/images/list-my-book.png" border=0 /></a></td>
                    <td><a href="/why-join.aspx"><img src="/images/why-join.png" border=0 /></a></td>
                    <td><a href="/join1.aspx"><img src="/images/join-now.png" border=0 /></a></td>
                </tr>
                </table>
            </div>
                
            <!-- lower content ***************************************************************************** -->            
            <div id="lower-content">            
                <div id="lower-content1">
                    <h2>Hey Authors -- Like Free Book Marketing?</h2>
                    <p>
                    We've been helping authors market their books for free since 1997. 
                     There weren't many online book marketing options back then.  
                     Today there are tons, but not many free ones.  Published.com gets thousands of monthly visitors.  
                     All you need is a published book with an ISBN (print or ebook) to market your book here.  
                     </p>
                     <img src="/images/arr1.gif" /> <a href="join1.aspx">Join Now</a>
                    
                </div>
                <div id="lower-content2">
                    <font size=2><i>And now a word from our sponsor:</i></font><br /><br />
                    <h2>An Ad Agency Just for Books</h2><br />
                    <a href="http://www.publiceye.com" target="_new"><img src="/images/sponsors/public-eye.png" border=0 align=left style="margin-right: 15px;margin-left: 10px; margin-top: 20px;margin-bottom: 10px;"  /></a>
                    <h3>We're Named Public Eye for a Reason.</h3>
                    <a href="http://www.publiceye.com" target="_new">PublicEye.com</a> gets your book in the public eye.  
                    We handcraft book marketing strategies combining social networking, search engine marketing, and traditional media. <br />
                    <span style="float: right; margin-right: 13px; margin-top: 13px;">
                    <a href="/Why-you-should-be-marketing-your-book-online.aspx">Learn More About Book Marketing</a> <img src="/images/arr1.gif" />
                    </span>
                    
                </div>
                <div id="lower-content3">
                    <h2>An Unpretentious Book Directory Treating Book Lovers To The Best Books</h2>
                    <img src="/images/spacer.gif" height=10 border=0 /><br />
The best books aren't always found in the jungle (yeah, that jungle with the rain forest and the big river).  Most book directories are controlled by huge corporations that only promote books everyone has heard of.   We're a different kind of book directory.  Published.com exists to help all authors and publishers build a buzz for their books while rewarding book lovers just for loving books.
                    
                    <br /><img src="/images/spacer.gif" height=15 border=0 /><br />
                    
                    <h3>Published.com Has Literary Delicacies For All</h3>
                    <ul>
                    <li>Contests for Readers to Win Books</li>
                    <li>Authors & Publishers Can Link Directly to up to 5 Online Retailers</li>
                    <li>Bloggers Can Interview Authors & Get Free Review Copies of Books</li>
                    <li>Book lovers can win trips to book signings</li>
                    <li>Authors & Publishers can get their books featured on our other genre specific book directories.</li>                    
                    </ul>
                    <p></p>
                  
                    <table width="98%">
                    <tr><td align=center><img src="/images/arr1.gif" /> <a href="/why-join.aspx">Check out more reasons to join the world's<br />most refreshing book directory.</a></td></tr>
                    </table>
                   


                </div>
            </div>
            
            

<!-- footer (start) -->

<div style="position: relative; top: 25px; height:1px; border: solid 0px #000;"> 
    <div style="position: relative; margin-left:auto; margin-right:auto; width:1006px;">    
        <div id="lower-footer" class="lower-header">
            <div id="About-Published.com" class="published_links" style="position: absolute;">
                <h4>About Published.com</h4>
                <a href="/about.aspx">Learn more about Published.com</a><br />
                <a href="/how-published-works.aspx">How Published.com works</a><br />
                <a href="/Privacy-Policy.aspx" >Published.com Privacy Policy</a><br />
                <a href="/sponsor.aspx" >Become a Published.com sponsor</a><br />
                <a href="/contact-us.aspx" >Contact Us</a><br />                    
            </div>
            <div class="published_links" style="position: absolute;left:225px;">
                <h4>Book Directory</h4>
                <a href="/join1.aspx" >List your book on Published.com</a><br />
                <a href="/Be-Featured-on-the-Genre-Home-Page.aspx">Be featured on Genre home pages</a><br />
                <a href="/published-search.aspx" >Search the Published.com book directory</a><br />
                <a href="/published-search.aspx?catid=3" >Book Bloggers</a><br />          
                <a href="/published-search.aspx?catid=394" >Podcasters</a><br />
            </div>
            <div id="Book Marketing" class="published_links" style="position: absolute;left:455px;">
                <h4>Book Marketing</h4>
                <a href="/Why-you-should-be-marketing-your-book-online.aspx" >Online Book Marketing 101</a><br />
                <a href="/making-amazon-work-for-you.aspx" >Make Amazon work for you</a><br />
                <a href="/Promoting-Your-Book-Using-Twitter.aspx">Promoting Your Book Using Twitter</a><br />  
            </div>
            <div id="Book Publishing & Printing" class="published_links" style="position: absolute;left:700px;">
                <h4>Book Publishing & Printing</h4>                
                <a href="http://www.book-publishers-compared.com" target="_blank">The Best Self-Publishing Companies</a><br />                
                <a href="#" >Book Publishing</a><br />
                <a href="/book-printing.aspx" >Book Printing</a><br />
            </div>
            <div style="position: absolute;top:32px;width:100%; height:1px; background-color:#0c9090;"></div>            
        </div>
        <div style="text-align: center; height: 50px; font-size: 8pt; color:#6c6c6c; ">&copy; 1997-2009 Hillcrest Publishing Group</div>
    </div>
</div>

<!-- footer (end) -->
        </div>
        <!-- start: book scroller ************************************************* -->
        <div id="book-scroller-container" style="position: relative; margin-left:auto; margin-right:auto; width:1006px; top:-934px; border: 0px solid #000;">
             <div id="featured-books" style="position: absolute; left: 14px; width:620px; border: 0px solid #000;">
                <!-- navigator -->
                <div style=" height:6px;"></div>

                <!-- prev link -->
                <a class="prev"></a>

                <!-- root element --> 
                <div class="scrollable">
                    <!-- for a change we have different kind of container for items --> 
                    <div id="thumbs">                      
                        <!-- root element for the items --> 
                       
                        <div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/4011-large.jpg" rel="#bid0" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid0">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Neikos-Five-Land-Adventure_4011.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2932-large.jpg" rel="#bid1" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid1">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Smoke_2932.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/82214-large.jpg" rel="#bid2" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid2">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Mrs-Ogg-Played-the-Harp_82214.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2934-large.jpg" rel="#bid3" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid3">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-0-to-60_2934.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2936-large.jpg" rel="#bid4" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid4">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Your-Business-and-Your-Life-Strategies-for-Professionals_2936.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2930-large.jpg" rel="#bid5" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid5">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Brother_2930.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/10775-large.jpg" rel="#bid6" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid6">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Street-Therapy-Interactive_10775.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2937-large.jpg" rel="#bid7" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid7">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-The-Amen-Heresy_2937.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2967-large.jpg" rel="#bid8" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid8">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Chemo-KateLynn-Humorous-Perspectives-on-Life-Before-Cancer-and-After-Diagnosis_2967.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2966-large.jpg" rel="#bid9" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid9">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Who-Says-I-Cant_2966.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2931-large.jpg" rel="#bid10" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid10">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Raise-the-Bottom-How-to-Keep-Secret-Alcoholics-from-Damaging-Your-Business_2931.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2938-large.jpg" rel="#bid11" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid11">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-The-Marriage-First-Aid-Kit_2938.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/11046-large.jpg" rel="#bid12" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid12">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-The-Newbie-Authors-Survival-Guide_11046.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/88374-large.jpg" rel="#bid13" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid13">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Natural-Cures-and-Remedies-That-Can-Literally-Save-Your-Life_88374.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/9465-large.jpg" rel="#bid14" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid14">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Convergence_9465.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/6783-large.jpg" rel="#bid15" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid15">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Pieces-Missing---A-Familys-Journey-of-Recovery-from-Traumatic-Brain-Injury_6783.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2933-large.jpg" rel="#bid16" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid16">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Painted-Faces_2933.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2939-large.jpg" rel="#bid17" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid17">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-The-Fine-Print-of-Self-Publishing_2939.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2968-large.jpg" rel="#bid18" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid18">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Defending-the-Enemy_2968.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/11344-large.jpg" rel="#bid19" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid19">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-A-Demon-Lies-Within_11344.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div><div><div style="position: relative;background-image: url('/images/3d-book.png'); background-repeat: no-repeat; width:194; height: 280px;"><img src="/images/books/2935-large.jpg" rel="#bid20" style="position: relative;top:11px;left:6px;width:160px;height:264px"  /></div><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="#" rel="#bid20">Click to Enlarge</a> <img src="/images/arr1.gif" /></span><br /><span style="margin-left: 5px; position: relative; top:3px; font-family:verdana;"><a href="/b-Meet-Me-on-the-Paisley-Roof_2935.aspx">Book Details</a> <img src="/images/arr1.gif" /></span></div>
                            
                    </div>          
                </div>

                <!-- next link -->
                <a class="next"></a>

                <!-- let rest of the page float normally -->
                <br clear="all" />
            </div>
        </div>
        <!-- end: book scroller ************************************************* -->
        
       
    </div>

    

<!-- Main top gnav section (start) -->

<link rel="stylesheet" type="text/css" href="/css/menu_style.css" />
<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="/css/ie6.css" media="screen"/>
<![endif]-->

<div style="position:absolute; top: 20px; width:100%;height:1px; z-index: 100;  ">
    <div style="top: 0px; width:1006px; margin-left:auto; margin-right: auto; ">
    
    <div style="position: relative; top: 20px; border: solid 0px #000; ">
        <a href="/"><img src="/images/published-logo.gif" style="position: relative; top: 10px; left: 10px;" border=0 /></a>
        <div id="topbuttons" style="position: absolute; top: 0px; left: 532px; border: solid 0px #000; width:430px; line-height: 10px;">            
                <div style=" position: absolute; color:#6598b6;border: 0px solid #ec1356; width: 250px;">
                    <div id="topgnav1_loggedout_panel">
	
                        <div style="position: relative; left: 70px;">
                        <img src="/images/nav/join.gif" style="" /> <a href="/join1.aspx" style="position: relative; top: -9px;">Join</a>
                        <img src="/images/nav/login.gif" style="margin-left: 25px; position: relative;" /> <a href="/login/" style="position: relative; top: -9px;">Login</a>
                        </div>
                    
</div>
                    
                </div>
                <div style="position:absolute;top:2px; left: 250px; width: 200px; z-index: 1000;">
                    <input type=TextBox ID="searchbox" Width="155px"></asp:TextBox>
                    <input type="image" name="topgnav1$search_submit" id="topgnav1_search_submit" src="/images/nav/search.gif" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;topgnav1$search_submit&quot;, &quot;&quot;, false, &quot;&quot;, &quot;javascript:postSearch();&quot;, false, false))" style="border-width:0px;position:absolute;top:0px; left: 159px;" />
                    <!-- <a href="#"><img id="search_submit" style="position:absolute;top:0px; left: 159px;"  src="/images/nav/search.gif" onclick="s_click();" border=0 /></a> -->
                </div>
                <script type="text/javascript">
                    function postSearch() {
                        location.href = "/published-search.aspx?searchstr=" + document.getElementById('searchbox').value;
                    }
                </script>
        </div>        
        <img src="/images/header/home1.png" style="position: relative; left: 110px; top: 20px;" />
    
        <!-- gnav (start) -->
        <div class="nav-wrapper">
            <div class="nav">
                <ul id="navigation">
                    <li>
	                    <a href="/">
		                    <span class="menu-left"></span>
		                    <span class="menu-mid">Home</span>
		                    <span class="menu-right"></span>
	                    </a>
                    </li>
                    <li>
	                    <a href="/published-search.aspx">
		                    <span class="menu-left"></span>
		                    <span class="menu-mid">Directory</span>
		                    <span class="menu-right"></span>
	                    </a>
   	                    <div class="sub">
		                    <ul>
			                    <li><a href="/published-search.aspx">All Categories</a></li>
			                    <li><a href="/published-search.aspx?catid=31">Fiction Books</a></li>
			                    <li><a href="/published-search.aspx?catid=67">Non-Fiction Books</a></li>
			                    <li><a href="/published-search.aspx?catid=71">Books We Love</a></li>			                    
			                    <!-- <li><a href="/published-search.aspx?catid=71">Recently Added</a></li> -->
		                    </ul>
		                    <div class="btm-bg"></div>
	                    </div>
                    </li>
                    <li>
	                    <a href="/published-search.aspx">
		                    <span class="menu-left"></span>
		                    <span class="menu-mid">Resources</span>
		                    <span class="menu-right"></span>
	                    </a>
   	                    <div class="sub">
		                    <ul>
			                    <li><a href="/published-search.aspx?catid=73">All Resources</a></li>
			                    <li><a href="/published-search.aspx?catid=13">Book Distributor</a></li>
			                    <li><a href="/published-search.aspx?catid=14">Book Cover Designer</a></li>
			                    <li><a href="/published-search.aspx?catid=217">Book Printing</a></li>
			                    <li><a href="/published-search.aspx?catid=218">PR / Marketing</a></li>
			                    <li><a href="/published-search.aspx?catid=220">Print Design</a></li>
			                    <li><a href="/published-search.aspx?catid=11">Self-Publishing Company</a></li>
			                    <li><a href="/published-search.aspx?catid=373">Virtual Author Assistant</a></li>
			                    <li><a href="/published-search.aspx?catid=12">Web Site Designer</a></li>
			                    <li><a href="/published-search.aspx?catid=70">Resource (Other)</a></li>
		                    </ul>
		                    <div class="btm-bg"></div>
	                    </div>
                    </li>
                    
                    <li class="">
	                    <a href="/Why-you-should-be-marketing-your-book-online.aspx">
		                    <span class="menu-left"></span>
		                    <span class="menu-mid">Book Marketing</span>
		                    <span class="menu-right"></span>
	                    </a>
	                    <div class="sub">
		                    <ul>
			                    <li><a href="/Why-you-should-be-marketing-your-book-online.aspx">Online Marketing</a></li>			                    
			                    
		                    </ul>
		                    <div class="btm-bg"></div>
	                    </div>
                    </li>
                    <li class="">
	                    <a href="/why-join.aspx">
		                    <span class="menu-left"></span>
		                    <span class="menu-mid">Why Join<font face="arial">?</font></span>
		                    <span class="menu-right"></span>
	                    </a>
                    </li>               		   		
                    <li class="last">
	                    <a href="/contact-us.aspx">
		                    <span class="menu-left"></span>
		                    <span class="menu-mid">Contact</span>
		                    <span class="menu-right"></span>
	                    </a>
	                   
	                    <div class="sub_last">
		                    <ul>
			                    <li><a href="/about.aspx">Who We Are</a></li>
			                    <li><a href="/how-published-works.aspx">How It Works</a></li>
			                    <li><a href="/sponsor.aspx">Partnership / Sponsor</a></li>
		                    </ul>
		                    <div class="btm-bg"></div>
	                    </div>	                   
                    </li>
                </ul>
            </div>
        </div>
        <!-- gnav (end) -->
    
    </div>
    
    </div>
</div>
<!-- Main top gnav section (end) -->

    
    <!-- Featured books Content boxes -->
    
    <div class="overlay" id="bid0">
    
    <img src="/images/books/4011-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" />
    
     <b>Summary</b>:<br />The Indians and the Crackedskulls are locked in the turmoil of war.     <p>Neiko a warrior with the glistening title of ''the Chosen One'' finds herself facing a collection of enemies. Her archenemies, Raven and Bloodhawk, have come up with a scheme to destroy her reputation with the help of a phony Indian chieftain.    <p>However, during the unfolding of their plan, Neiko finds out that a land she thought she had only imagined is actually real - and more terrifying - than she ever imagined. It turns out that Ramses - the arch-villain of her collection - has sinister plans of his own for Neiko, whom he eventually traps in Qari.    
     
     <p>Neiko must find her way back home and turn the tables on her enemies, but what she finds out during her travels in Qari will change her entire existence for all eternity.<p></p><center><a href="/b-Neikos-Five-Land-Adventure_4011.aspx"><img src="/images/s_buttons1.png" border=0 /></a>
     </center>
     
     </div>
     
     
     
     <div class="overlay" id="bid1">
     
     
     <img src="/images/books/2932-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> 
     
     <b>Summary</b>:<br />
     
     Set in Miami and Central America during the 70s, Smoke is the story of a journey that begins when Jim Smyth, a young Marine returning from combat in Viet Nam, is brilliantly manipulated by an anonymous adversary with ties to the highest levels of U.S. government. Chosen for his unique skills, Smyth is forced into kidnapping the president of a large, well connected security company.    Two intriguing women become tangled in the kidnapping plot. One, like Smyth, is in the Witness Protection Program. The other is the sworn enemy of the kidnap victim and a major shareholder of the company. Smyth, code named Smoke by the Department of Justice, must rely on his military training and survival instincts to ensure they live through the bizarre plot and bring down their enemy.    The lies and abuse of power by senior members of the U.S. government in Smoke parallel those in today s society. Jim Smyth must prove he is up to the challenge against an adversary of unsurpassed evil and intelligence; in this competition, losers die.<p>
     
     
     </p><center>
     
     <a href="/b-Smoke_2932.aspx">
     
     <img src="/images/s_buttons1.png" border=0 />
    </a>
     </center>
     
     </div>
     
     
     <div class="overlay" id="bid2">
     <img src="/images/books/82214-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> 
     <b>Summary</b>:<br /><p></p>
     <center>
     
     <a href="/b-Mrs-Ogg-Played-the-Harp_82214.aspx"><img src="/images/s_buttons1.png" border=0 /></a>
     
     </center>
     
     </div>
     
     <div class="overlay" id="bid3"><img src="/images/books/2934-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:
     
     
     <br />
     
     O TO 60 is Susan Slater s debut women s fiction novel with that all too true-to-life story of a woman dumped. Meet Shelly Sinclair, matriarch of the quintessential American family.   Shelly s life is perfect one most women would envy a 35 year marriage to a successful doc, two grown sons, a beautiful home and the security of predictability. Without warning, this world collapses. Her husband of a lifetime announces that he s asked someone to marry him a someone thirty-nine years his junior. And, oh yes, the mother of his four-year-old child. Alone. To start over. Sixty is a long way from sixteen. Is Prince Charming still out there? But isn t there more? Her quest to find self is as endearing as it is enduring. Because maybe, just maybe these are the best years of her life.<p></p>
     
     <center><a href="/b-0-to-60_2934.aspx">
     
     <img src="/images/s_buttons1.png" border=0 />
     
     </a></center></div><div class="overlay" id="bid4"><img src="/images/books/2936-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />Your Business and Your Life covers four important, and often overlooked, areas that many professionals struggle with daily time and efficiency, burnout, management, and marketing and offers practical advice that can be applied immediately.     Contrary to the tone of most business books, Dr. McCallister treats you like a colleague rather than a student forced to endure a lecture. Having spent hundreds of hours counseling and coaching business professionals, he knows to skip the business school jargon and relate his concepts to real-world situations.    More than a how-to guide, Your Business and Your Life addresses the connection between personal and professional life, and offers strategies for finding happiness at work and home.    Of course, it isn t always possible to remain happy and positive, just as it isn t possible to succeed at every endeavor. Thankfully, Dr. McCallister offers illuminating advice on how to find the light of new opportunities during the darkness of disaster.     With Your Business and Your Life you ll benefit from years of in-the-field experience and learn all the things they never taught you in school.<p></p><center><a href="/b-Your-Business-and-Your-Life-Strategies-for-Professionals_2936.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid5"><img src="/images/books/2930-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />What does it mean to call another man brother? And how often are we faced with circumstances that truly test the nature or our relationships? When attorney Chase Riordan and his estranged brother Jared are accused of crimes they didn t commit, they must somehow work together to clear their names. As they peel away the layers of deception to uncover the truth, suspicions fall upon those closest to them, testing relationships and forcing choices between their families, their careers, and their friendships. Against a backdrop of suspicion, betrayal, revenge, and murder, the men must decide how much they re willing to risk to attain justice, and whether the bonds of brotherhood extend beyond blood. For in the end, their survival and fate may well depend upon their belief and trust in each other.<p></p><center><a href="/b-Brother_2930.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid6"><img src="/images/books/10775-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br /><p>How to guide for how to co-construct cooperation with troubled youth.</p><p>Co-constructing cooperation with troubled adolescents, minorities, students who receive special education, Teens from non-traditional households, involuntary or mandated juvenile clients, gang members and students from traditionally lower performing groups.</p><p></p><center><a href="/b-Street-Therapy-Interactive_10775.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid7"><img src="/images/books/2937-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />Ex-priest and dyslexic expert of ancient languages, Jack Fisher, agrees to assist an Israeli friend and expert of religious history and is drawn into an unsolved mystery of the Dead Sea Scrolls. His discovery of the true meaning of the Copper Scroll threatens to expose the three religions of Abraham as the monotheistic legacy of an ancient, sun-worshipping pharaoh, Amen-hotep IV. Jack joins forces with Agent Ariel Hadar of the Israeli Antiquities Authority and Sami Salaa, a Palestinian boy of the streets, in an epic struggle for possession of the scroll, and an explosive truth which will upend religious history and shatter the religious legacy of Jews, Christians and Muslims.<p></p><center><a href="/b-The-Amen-Heresy_2937.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid8"><img src="/images/books/2967-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />Want to raise the spirits of a woman battling cancer? <i>Chemo KateLynn: Humorous Perspectives on Life Before Cancer and After Diagnosis</i> is the perfect prescription. Written by cancer survivor and humorist Andrea Lynn Katz, Chemo KateLynn, the fictional heroine in this book, illuminates the way one woman looks at different slices of life in her BC (before cancer) and AD (after diagnosis) days.<BR>This book is a unique gem in its ability to bring tactful comedic insight to one of the scariest and least approachable of all topics cancer. Its content goes way beyond the expected medical humor and covers topics such as office politics, family dynamics -- even food and shopping!<BR>With the wit of author Katz and the clever illustrations of professional artist Brandon Friend, Chemo KateLynn becomes an immediately endearing character. This book is not just a must buy it s a must laugh!<p></p><center><a href="/b-Chemo-KateLynn-Humorous-Perspectives-on-Life-Before-Cancer-and-After-Diagnosis_2967.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid9"><img src="/images/books/2966-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />Two words have the power to change a person s outlook: good...considering. Jothy Rosenberg has heard this his whole life, starting at age sixteen when bone cancer led to the amputation of his right leg. Three years later, when cancer forced the removal of a lung and essentially served as a death sentence, this epithet continued. Rosenberg grew tired of only being good considering his disability. In the decades since, he has used athletics to overcome this social stigma. He turned his disability into a <i>super</i>ability, often performing in challenging open water swims, cancer-fundraising bike rides, and treacherous skiing adventures better than two-leggers. And in the business world, when working in a reliable position failed to quench his need for risk taking, he plunged into entrepreneurship.<BR>In <i>Who Says I Can t?</i>, Rosenberg teaches by example how everyone can overcome life s obstacles. He shows that when the world says you can t, courage and determination prove you can be more than good...considering.<p></p><center><a href="/b-Who-Says-I-Cant_2966.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid10"><img src="/images/books/2931-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />Alcoholism is rampant, but due to its stigma, it is rarely discussed directly as a business issue. Raise the Bottom explains the real danger of alcoholism in the workplace -- the significant impact of alcoholic thinking and behavior, whether a person is under the influence or not. Raise the Bottom is a business book, not a recovery book. It covers Step Zero, the real beginning of any program of recovery.     Raise the Bottom examines how alcoholics are secretly responsible for many different business problems in a variety of situations. The book takes readers from the preconceptions and cynicism about alcoholism typical of the general public, to the point of connecting the dots between the disease, the behavior, and the bottom line - and to propose effective steps to a solution.    Part One discusses the problems created by alcoholism in the work environment, covering biological factors, definitions of alcoholism, and stages in the progress of the disease. Part Two shows how to identify alcoholics often long before the alcoholic himself is aware he has a problem. Part Three closes with concrete responses to the problem, including disenabling the alcoholic, and the other steps necessary to protect yourself and your business from the damage an alcoholic can cause and reduce the harm they do to themselves.<p></p><center><a href="/b-Raise-the-Bottom-How-to-Keep-Secret-Alcoholics-from-Damaging-Your-Business_2931.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid11"><img src="/images/books/2938-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />In his 35+ years as a therapist and marriage counselor, Bryce Kaye has come to know that problems in marriages are not going to be overcome by a self help book, a Marriage For Idiots handbook, or a couple of episodes of Dr. Phil in the afternoon. His work, The Marriage First Aid Kit, is just what the title suggests, a temporary help for couples until more permanent care can be obtained.  Dr. Kaye helps his audience work on issues resulting from communication avoidance by showing how to balance the conflicting needs of attachment and autonomy in a relationship. This vital balance is endangered not only by the obvious assassins abuse, affairs, and addictions, but also what Kaye terms hedonic inhibitions the inability by some partners to seek and enjoy fun in a couple s life together. Kaye employs everyday wisdom and therapeutic theory to show individuals in a relationship how to establish autonomy while affirming attachment, how to manage inevitable and healthy conflicts, and how to share power and responsibility throughout their marriage. Incorporating examples culled from his years of helping clients, Kaye peppers his book with problem scenarios to which readers can relate as well as with a useful variety of measurement tools and viable exercises to help couples through the common issues faced in intimate relationships.  Rising above the plethora of quick-fix, relationship-help manuals, The Marriage First Aid Kit by Bryce Kaye, PhD offers professional, intelligent suggestions to couples to be employed, not as panaceas, but as temporary help while they work their way through the difficulties of life together.<p></p><center><a href="/b-The-Marriage-First-Aid-Kit_2938.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid12"><img src="/images/books/11046-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />After crash landing into self publishing with nothing but the clothes she had on, author AK Taylor fought for survival with trying to market her books on a small budget. After two years of trial and error, reworking, refining, and reaching out, she has created the first survival guide for book marketing compiled of great tools and resources that can be used by any author during the rough times. Comparing the book marketing wilderness with the real wilderness is how Taylor viewed the publishing world around her. Growing up in the woods and learning survival skills has given her this unique viewpoint for a different kind of world. When she started her search for information, a book marketing survival guide didn't exist—until now.<p></p><center><a href="/b-The-Newbie-Authors-Survival-Guide_11046.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid13"><img src="/images/books/88374-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br /><p></p><center><a href="/b-Natural-Cures-and-Remedies-That-Can-Literally-Save-Your-Life_88374.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid14"><img src="/images/books/9465-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br /><p>Slung amongst the decomposing bodies and wreckage in the plague-stricken Chinese village was a cheery banner: To Tzu, Good fortune on your journey to America. This was it. This was all the evidence CDC researcher Sandra Stone needed to know that the deadly virus had jumped to humans—and it was headed to the United States.  </p>  <p> In the year 2020, the effects of global warming have made the Arctic Ocean free of ice. In the United States, Atlanta, Georgia, has been thrust into permanent winter, and never-ending rains over the heartland force survivors to escape to higher and higher ground. Simultaneously, overpopulation has made a perfect breeding ground for disease to cause deadly destruction. And it's here, threatening to crush global infrastructure and leave few survivors, the last relics of a golden age who must battle an ever-worsening climate.</p><p> In the year 2220, historian Jules Grogan speculates only a few million people are left. "Is it intrinsic in our genetic makeup," he wonders, "that we destroy ourselves as part of nature's design." Toggling between a future too close for comfort and a more distant age two hundred years after the collapse, Convergence follows scientists, politicians, soldiers and everyday people battling an environmental apocalypse and a catastrophic pandemic. In the future, survivors look to the past to understand how they inherited a decimated planet. But with ancient troubles resurfacing generations after the virus, humanity is left to question whether the human race is meant to survive.</p><p></p><center><a href="/b-Convergence_9465.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid15"><img src="/images/books/6783-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />In <i>Pieces Missing: A Family's Journey of Recovery from Traumatic Brain Injury</i>, author Larry Kerpelman tells his wife Joanie's story as it happens. You will watch and feel as she is hospitalized after a freak accident, faces dysfunction and possibly death, undergoes surgery, and, over the course of a year, recovers the pieces missing from her memory, speech, and confidence.    <p>This book is more than the story of one person's recovery from a brain injury. It is a story of how a marriage and family persevered and survived the biggest crisis of their lives. Woven into it are discussions of the inadequacies within the health care system that Larry and Joanie faced along with valuable information about traumatic brain injury for anyone dealing with the aftermath of such an injury.     <p>Whether you or a loved one has been affected by traumatic brain injury, or you wish to understand how the human spirit deals with adversity, this memoir of love, hope, family, and recovery will teach and inspire you.<p></p><center><a href="/b-Pieces-Missing---A-Familys-Journey-of-Recovery-from-Traumatic-Brain-Injury_6783.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid16"><img src="/images/books/2933-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />Fourteen murders across the country and the only thing that the victims have in common is that all are African-Americans. With the body count rising, the police, the FBI, and the Homeland Security s Domestic Terrorism Unit know that a message left at each crime scene not only links the murders, but suggests that they are hate crimes being committed by organized serial killers, their victims chosen at random.    As agent Jack Andresen delves deeper into the investigation, it becomes apparent that there is more to these random acts of violence than meets the eye. Jack soon realizes that these victims are being targeted to achieve a more sinister agenda. Links to terrorism and a very real threat of civil war begin to unfold as the horrific facts are revealed one by one. Time is running out for Jack as the fight for global power begins with the bloodshed of those chosen to be sacrificed first.<p></p><center><a href="/b-Painted-Faces_2933.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid17"><img src="/images/books/2939-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br /><i>The Fine Print of Self-Publishing</i>, now in its fourth edition, has been lauded by industry professionals as a "must read" for any author considering self-publishing.<p> <i>The Fine Print of Self-Publishing</i> (Third Edition) was awarded the Gold Medal for best writing/publishing book in 2009 by the Independent Publisher Book Awards.<p><p></p><center><a href="/b-The-Fine-Print-of-Self-Publishing_2939.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid18"><img src="/images/books/2968-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br /><i>Defending the Enemy</i> is an eyewitness account of an extraordinary time in America's history - the "Tokyo Trials."<BR>From 1946-48 Elaine B. Fischel worked in Tokyo alongside the American attorneys assigned to defend the Japanese war criminals held responsible for the torture and deaths of millions of civilians and prisoners of war. She recounts the post-WWII transition in Japan to the country's occupation by their former enemy, and the subsequent surprise on the part of the Japanese citizenry that the U.S. allegiance to democracy meant providing a fair trial even to the men considered the most evil perpetrators of atrocities. In letters to her family at the time, the author as a young woman tries to explain her relationships with the defendants and her own surprise at the growing fondness she felt for many of the "villains" of WWII-particularly prime minister and general Hideki Tojo, known during the war as "Razor."<BR>  <i>Defending the Enemy</i> is also the story of a young woman who wants to make the most of her time in a country so full of beauty. Fischel interweaves the activities and intrigues of the trial alongside her tales of travel throughout Japan, her social engagements with high-ranking military and civilians, and her unique enduring relationships, such as her friendship with Emperor Hirohito's brother, Prince Takamatsu. In doing so, Fischel illuminates the paradoxes inherent during this period in history.<p></p><center><a href="/b-Defending-the-Enemy_2968.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid19"><img src="/images/books/11344-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br /><p><strong>The powers of hell that lie within are very real&hellip;</strong></p><p>Following his murder, Andrew McMurray finds himself transported to the depths of hell. There he is indoctrinated as an apprentice to a demon master, Sonneillon. Exposing Andrew to the dark reaches of his evil powers, Sonneillon demonstrates a demons&rsquo; ability to possess, torment and control the thoughts and actions of the living. What is Andrew&rsquo;s ultimate goal following his apprenticeship? Revenge on his wife, Katelyn and their nine-year-old son, Joshua, both of whom he holds responsible for his murder.</p><p>As Katelyn rebuilds her life following her husbands&rsquo; death, she meets Michael Gordon, a recent escapee from Corporate America who has his own troubled, tragic past. As their relationship grows, they realize they share something more than their burgeoning love for one another &mdash; the powers of hell have deeply impacted both their pasts. Evil continues to infiltrate itself into the pair's lives, bringing with it haunting and unspeakable horrors.</p><p>Andrew's plan of revenge begins to materialize once he inhabits Joshua, having him act out in often violent and disturbing ways. With possession of Joshua&rsquo;s mind and body complete, will hell's ultimate evil goals come to pass, or can the local priest of a small Maine town exorcize Father from Son? At stake, doom-laden repercussions for all involved&hellip;and perhaps the world as a whole.</p><p></p><center><a href="/b-A-Demon-Lies-Within_11344.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div><div class="overlay" id="bid20"><img src="/images/books/2935-large.jpg" align=left style="border: 3px solid #efefef; margin: 0px 10px 5px 5px;" /> <b>Summary</b>:<br />Trussell Jones has a problem. He is crazy in love with a beautiful girl named Ellen. The problem? He has no car. His stepmother, who believes that she is spiritually connected to Queen Victoria, won t let him drive. Furthermore, she is afraid Trussell is trying to kill her. Not to be overlooked is the fact that Trussell is being pursued by a gang of armed redneck motorcycle hoods, while his neighbors are preoccupied with changing visions of St. Francis. Just another heartwarming tale of a boy in love with a girl? Hardly.    This delightfully quixotic coming-of-age story, set in Columbus, Georgia in the 1950s, truly has something to shock and beguile even the most jaded reader. Its irreverent protagonist will take you on a road trip of hits, near misses, twists, and sudden turns that ll set you on your ear. You ll be unable to put the book down, until you reach its charming yet totally unpredictable conclusion.<p></p><center><a href="/b-Meet-Me-on-the-Paisley-Roof_2935.aspx"><img src="/images/s_buttons1.png" border=0 /></a></center></div>
   

    <script>
         // execute your scripts when DOM is ready. this is a good habit 
         $(function() {

             // initialize scrollable  
             $("div.scrollable").scrollable({
                 size: 3,
                 items: '#thumbs',
                 hoverClass: 'hover'
             });
         });

         $(function() {
             $("img[rel]").overlay({ top: 150 });
             $("a[rel]").overlay({ top: 150 });
         });
    </script>
    <!-- Scroller and Overlay Section (end) -->
    
    </form>
    
    <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
    try {
    var pageTracker = _gat._getTracker("UA-11017104-1");
    pageTracker._trackPageview();
    } catch (err) { }
    </script>
    

</body>
</html>
